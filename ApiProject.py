import pygame, requests, sys, os, math
from pprint import pprint


class Label:
    def __init__(self, rect, text, text_color='grey', background_color='blue'):
        self.rect = pygame.Rect(rect)
        self.text = text
        if background_color != '-1':
            self.bgcolor = pygame.Color(background_color)
        else:
            self.bgcolor = None
        self.font_color = pygame.Color(text_color)
        # Рассчитываем размер шрифта в зависимости от высоты
        self.font = pygame.font.Font(None, self.rect.height - 4)
        self.rendered_text = None
        self.rendered_rect = None

    def render(self, surface):
        if self.bgcolor:
            surface.fill(self.bgcolor, self.rect)
        self.rendered_text = self.font.render(self.text, 10, self.font_color)
        self.rendered_rect = self.rendered_text.get_rect(x=self.rect.x + 2, centery=self.rect.centery)
        # выводим текст
        surface.blit(self.rendered_text, self.rendered_rect)


class Button(Label):
    def __init__(self, rect, text):
        super().__init__(rect, text)
        self.bgcolor = pygame.Color('blue')
        # при создании кнопка не нажата
        self.pressed = False
        self.click = False
        self.mouse_on = False

    def render(self, surface):
        self.click = False
        if self.mouse_on:
            surface.fill(pygame.Color('lightblue'), self.rect)
        else:
            surface.fill(self.bgcolor, self.rect)
        self.rendered_text = self.font.render(self.text, 1, self.font_color)
        text_width = self.rendered_text.get_rect().width
        if text_width > self.rect.width:
            width_delta = text_width / self.rect.width
            text = self.text[:int(len(self.text) / width_delta) - 2]
            text += '...'
            self.rendered_text = self.font.render(text, 1, self.font_color)
        if not self.pressed:
            color1 = pygame.Color('white')
            color2 = pygame.Color('black')
            self.rendered_rect = self.rendered_text.get_rect(x=self.rect.x + 5, centery=self.rect.centery)
        else:
            color1 = pygame.Color('black')
            color2 = pygame.Color('white')
            self.rendered_rect = self.rendered_text.get_rect(x=self.rect.x + 7, centery=self.rect.centery + 2)

        # рисуем границу
        pygame.draw.rect(surface, color1, self.rect, 2)
        pygame.draw.line(surface, color2, (self.rect.right - 1, self.rect.top), (self.rect.right - 1, self.rect.bottom),
                         2)
        pygame.draw.line(surface, color2, (self.rect.left, self.rect.bottom - 1),
                         (self.rect.right, self.rect.bottom - 1), 2)
        # выводим текст
        surface.blit(self.rendered_text, self.rendered_rect)

    def get_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.pressed = self.rect.collidepoint(*event.pos)
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.pressed = False
            self.click = self.rect.collidepoint(*event.pos)
        elif event.type == pygame.MOUSEMOTION:
            if self.rect.collidepoint(*event.pos):
                self.mouse_on = True
            else:
                self.mouse_on = False

        if self.pressed:
            return True


class TextBox(Label):
    def __init__(self, rect, text, max_len=None):
        super().__init__(rect, text, background_color='white')
        self.active = False
        self.blink = True
        self.blink_timer = 0
        self.cursor_position = 0
        self.cursor_pixels_pos = 0
        self.max_len = max_len

    def get_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                self.get_symb_by_click(event.pos)
        if event.type == pygame.KEYDOWN and self.active:
            if event.key in (pygame.K_RETURN, pygame.K_KP_ENTER):
                self.execute()
            elif event.key == pygame.K_BACKSPACE:
                if len(self.text) > 0 and self.cursor_position != 0:
                    # print(self.text, end=' ')
                    text = self.text[:self.cursor_position - 1]
                    # print(text, end=' ')
                    text += self.text[self.cursor_position:]
                    # print(text)
                    self.text = text
                    self.cursor_position -= 1
            elif event.key == pygame.K_TAB:
                pass
            elif event.key == pygame.K_LEFT:
                if self.cursor_position != 0:
                    self.cursor_position -= 1
                    # print(self.cursor_position)
            elif event.key == pygame.K_RIGHT:
                if self.cursor_position <= len(self.text) - 1:
                    self.cursor_position += 1
            else:
                if event.unicode not in 'abcdefghijklmnopqrstuvwxyz' \
                                        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
                                        'абвгдеёжзийклмнопрстуфхцчшщъыьэюя' \
                                        'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ' \
                                        ',.1234567890 ':
                    print('Invalid symbol:', event.unicode)
                    return

                if pygame.K_LSHIFT in pygame.key.get_pressed():
                    symb = event.unicode.upper()
                else:
                    symb = event.unicode

                if symb == '':
                    return

                if len(self.text) == self.max_len:
                    return
                if not self.max_len:
                    lasts = self.rect.width - self.rendered_rect.width
                    newsymb = self.font.render(symb, 1, self.font_color).get_rect().width
                    if newsymb + 5 > lasts:
                        return

                if self.cursor_position != len(self.text):
                    text = self.text[:self.cursor_position]
                    # print(text, end=' ')
                    text += symb
                    # print(text, end=' ')
                    text += self.text[self.cursor_position:]
                    # print(text)
                    self.text = text
                else:
                    self.text += symb

                if symb != '':
                    self.cursor_position += 1

        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.active = self.rect.collidepoint(*event.pos)

        if self.active:
            return True

    def get_symb_by_click(self, pos):
        x, y = pos
        if self.rect.collidepoint(x, y):
            x -= self.rect.x
            for ind, symb in enumerate(self.text):
                newsymb = self.font.render(symb, 1, self.font_color).get_rect().width
                x -= newsymb
                if x <= 0:
                    self.cursor_position = ind
                    break
            if x >= 0:
                self.cursor_position = len(self.text)

    def update(self):
        if pygame.time.get_ticks() - self.blink_timer > 200:
            self.blink = not self.blink
            self.blink_timer = pygame.time.get_ticks()

    def render(self, surface):
        super(TextBox, self).render(surface)
        if self.blink and self.active:
            ind = 0
            self.cursor_pixels_pos = 0
            while self.cursor_position != len(self.text) + ind:
                ind -= 1
                try:
                    symb = self.text[ind]
                except IndexError:
                    self.cursor_position = 0
                    break
                rendered_symb = self.font.render(symb, 1, self.font_color)
                symb_rect = rendered_symb.get_rect()
                self.cursor_pixels_pos += symb_rect.width + 2
            x_pos = self.rendered_rect.right - 2 - self.cursor_pixels_pos
            pygame.draw.line(surface, pygame.Color('black'),
                             (x_pos, self.rendered_rect.top + 2),
                             (x_pos, self.rendered_rect.bottom - 2))

    def execute(self):
        self.active = False
        search(self.text)


class GUI:
    def __init__(self):
        self.elements = []

    def add_element(self, element):
        self.elements.append(element)

    def render(self, surface):
        for element in self.elements:
            render = getattr(element, 'render', None)
            if callable(render):
                element.render(surface)

    def update(self):
        for element in self.elements:
            update = getattr(element, 'update', None)
            if callable(update):
                element.update()

    def get_event(self, event):
        ui_hovered = False
        for element in self.elements:
            get_event = getattr(element, 'get_event', None)
            if callable(get_event):
                collided = element.get_event(event)
                if collided:
                    ui_hovered = True
        return ui_hovered


def draw(x, y, spn, layers, metka):  # Функция для отрисовки карты
    response = None
    try:
        url = 'https://static-maps.yandex.ru/1.x/'
        if metka is None:
            map_request = '{}?ll={},{}&spn={}&size=600,450&l={}'.format(url, y, x, spn, layers)
        else:
            map_request = '{}?ll={},{}&spn={}&size=600,450&l={}&pt={},pm2rdm'.format(url, y, x, spn, layers, metka)
        response = requests.get(map_request)
        # print(map_request)
        if not response:
            print('Ошибка выполнения запроса:')
            # print(geocoder_request)
            print('Http статус:', response.status_code, '(', response.reason, ')')
            sys.exit(1)
    except:
        print('Запрос не удалось выполнить. Проверьте наличие сети Интернет.')
        sys.exit(1)

    # Запишем полученное изображение в файл.
    map_file = 'map.png'
    try:
        with open(map_file, 'wb') as file:
            file.write(response.content)
    except IOError as ex:
        print('Ошибка записи временного файла:', ex)
        sys.exit(2)

    # Инициализируем pygame
    # screen = pygame.display.set_mode((600, 450))
    global screen, MAP_LAST
    MAP_LAST = pygame.image.load(map_file)
    # Рисуем картинку, загружаемую из только что созданного файла.
    screen.blit(MAP_LAST, (0, 0))
    # Переключаем экран и ждем закрытия окна.
    pygame.display.flip()

    # Удаляем за собой файл с изображением.
    os.remove(map_file)


def draw_object(point_x, point_y):
    global X, Y, SPN, LAYER, METKA, POSTAL_NEEDED, ORG_FOUND, lbl_full_name
    ORG_FOUND = False
    METKA = '{},{}'.format(point_y, point_x)
    res = get_obj_by_coords(point_x, point_y)
    if res:
        draw(X, Y, SPN, LAYER, METKA)
        addr, postal = res
        if not POSTAL_NEEDED or postal == '':
            postal = ''
        else:
            postal = ', ' + postal
        lbl_full_name.text = addr + postal


def draw_org(point_x, point_y):
    global X, Y, SPN, LAYER, METKA, ORG_FOUND
    METKA = '{},{}'.format(point_y, point_x)
    draw(X, Y, SPN, LAYER, METKA)
    res = get_org_by_coords(point_x, point_y)
    if res:
        org_name, org_address, point, dist = res
        lbl_full_name.text = '{}м: {}; {}'.format(str(int(dist)), org_name, org_address)
        ORG_FOUND = True
    else:
        lbl_full_name.text = 'Ничего не найдено!'
        ORG_FOUND = False


def search(string, no_draw=False, code_only=False):  # Фунция для поиска объекта(в нее нужно просто передать название)
    global X, Y, LAYER, METKA, SPN, POSTAL_NEEDED, ORG_FOUND, lbl_full_name
    ORG_FOUND = False
    geocoder_request = 'http://geocode-maps.yandex.ru/1.x/?geocode={}, 1&format=json'.format(string)
    response = None
    try:
        response = requests.get(geocoder_request)
        if response:
            json_response = response.json()
            toponym = json_response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']
            toponym_address = toponym['metaDataProperty']['GeocoderMetaData']['text']
            try:
                keys = ['metaDataProperty', 'GeocoderMetaData', 'AddressDetails', 'Country',
                        'AdministrativeArea', 'SubAdministrativeArea', 'Locality', 'Thoroughfare',
                        'Premise', 'PostalCode', 'PostalCodeNumber']
                prev = toponym
                for key in keys:
                    prev = prev[key]
                toponym_postal_code = prev
            except Exception as e:
                # print('Postal code not found:', e)
                toponym_postal_code = ''

            lbl_full_name.text = toponym_address
            lbl_full_name.text += ' ' + toponym_postal_code if POSTAL_NEEDED else ''

            if not code_only:
                toponym_coodrinates = toponym['Point']['pos']
                spn = toponym['boundedBy']['Envelope']
                spn1 = list(map(float, spn['lowerCorner'].split()))
                spn2 = list(map(float, spn['upperCorner'].split()))
                SPN = '{},{}'.format(abs(spn1[0] - spn2[0]), abs(spn1[1] - spn2[1]))
                Y, X = [float(i) for i in toponym_coodrinates.split()]
                METKA = '{},{}'.format(str(Y), str(X))

            if no_draw:
                pass
            else:
                draw(X, Y, SPN, LAYER, METKA)
            return toponym_address, toponym_postal_code
        else:
            print('Ошибка выполнения запроса:')
            print(geocoder_request)
            print('Http статус:', response.status_code, '(', response.reason, ')')
    except:
        print('Запрос не удалось выполнить. Проверьте наличие сети Интернет.')


def handle_key_down(i, coef=1):  # Функция для обработки действий
    global X
    global Y
    global SPN
    global LAYER
    global METKA
    if i == pygame.K_PAGEDOWN:
        spn_x, spn_y = map(float, SPN.split(','))
        spn_x += coef * spn_x
        spn_y += coef * spn_y
        if spn_x <= 180 and spn_y <= 90:
            SPN = '{},{}'.format(spn_x, spn_y)
        else:
            spn_x = 180
            spn_y = 90
            SPN = '{},{}'.format(spn_x, spn_y)
    if i == pygame.K_PAGEUP:
        spn_x, spn_y = map(float, SPN.split(','))
        spn_x /= 2
        spn_y /= 2
        spn_x = max(0.0002, spn_x)
        spn_y = max(0.0002, spn_y)
        if spn_x >= 0 and spn_y >= 0:
            SPN = '{},{}'.format(spn_x, spn_y)
        else:
            spn_x = 0.0002
            spn_y = 0.0002
            SPN = '{},{}'.format(spn_x, spn_y)
    if i == pygame.K_UP:
        X += 0.5 * list(map(float, SPN.split(',')))[1]
    if i == pygame.K_DOWN:
        X -= 0.5 * list(map(float, SPN.split(',')))[1]
    if i == pygame.K_LEFT:
        Y -= 0.5 * list(map(float, SPN.split(',')))[0]
    if i == pygame.K_RIGHT:
        Y += 0.5 * list(map(float, SPN.split(',')))[0]
    if -200 <= X <= 200 and -100 <= Y <= 100:
        draw(X, Y, SPN, LAYER, METKA)
    else:
        print('Invalid X, Y:', X, Y)


def get_ll_by_mouse(mouse_x, mouse_y):
    spn_real = [0.001, 0.004, 0.008, 0.016, 0.031, 0.065, 0.13, 0.25, 0.5, 1.0, 2.05, 4.05, 8.05, 17.0, 32.0]
    # Special coefs for map
    spn_x_coefs = {0.001: 1.209, 0.004: 1.213, 0.008: 1.213, 0.016: 1.213, 0.031: 1.213, 0.065: 1.213, 0.13: 1.213,
                   0.25: 1.213, 0.5: 1.213, 1.0: 1.213, 2.05: 1.213, 4.05: 1.213, 8.05: 1.213, 17.0: 1.213, 32.0: 1.213}
    spn_y_coefs = {0.001: 0.93, 0.004: 1.861, 0.008: 1.861, 0.016: 1.861, 0.031: 1.861, 0.065: 1.861, 0.13: 1.861,
                   0.25: 1.861, 0.5: 1.861, 1.0: 1.861, 2.05: 1.861, 4.05: 1.861, 8.05: 1.861, 17.0: 1.861, 32.0: 1.861}
    # =====================
    global X, Y, SPN, METKA, lbl_full_name
    center_x, center_y = pygame.display.get_surface().get_rect().center
    spn_y, spn_x = get_real_spn(*map(float, SPN.split(',')))

    x_coef = spn_x_coefs[spn_x]
    # print(x_coef)
    y_coef = spn_y_coefs[spn_y]

    map_y, map_x = map(float, SPN.split(','))
    # print(spn_x, spn_y, map_x - spn_x, map_y - spn_y)

    dx, dy = x_coef * (mouse_x - center_x), y_coef * (mouse_y - center_y)
    point_x = X - spn_x / 2 / center_x * dy
    point_y = Y + spn_y / 2 / center_y * dx
    # print(X, Y, dx, dy, point_x, point_y)
    # X, Y = point_x, point_y
    return point_x, point_y


def get_real_spn(spn_x, spn_y):
    spn_real = [0.001, 0.004, 0.008, 0.016, 0.031, 0.065, 0.13, 0.25, 0.5, 1.0, 2.05, 4.05, 8.05, 17.0, 32.0]
    spn_x_real, spn_y_real = 0, 0
    min_diff_x, min_diff_y = 32.0, 32.0
    for spn_normal in spn_real:
        diff_x = abs(1 - spn_x / spn_normal)
        diff_y = abs(1 - spn_y / spn_normal)
        if diff_x < min_diff_x:
            min_diff_x = diff_x
            spn_x_real = spn_normal
        if diff_y < min_diff_y:
            min_diff_y = diff_y
            spn_y_real = spn_normal

    return spn_x_real, spn_y_real


def lonlat_distance(a, b):
    degree_to_meters_factor = 111 * 1000  # 111 километров в метрах
    a_lon, a_lat = a
    b_lon, b_lat = b

    # Берем среднюю по широте точку и считаем коэффициент для нее.
    radians_lattitude = math.radians((a_lat + b_lat) / 2.)
    lat_lon_factor = math.cos(radians_lattitude)

    # Вычисляем смещения в метрах по вертикали и горизонтали.
    dx = abs(a_lon - b_lon) * degree_to_meters_factor * lat_lon_factor
    dy = abs(a_lat - b_lat) * degree_to_meters_factor

    # Вычисляем расстояние между точками.
    distance = math.sqrt(dx * dx + dy * dy)

    return distance


def get_obj_by_coords(point_x, point_y):
    point_x = max(min(85.0, float(point_x)), -85.0)
    point_y = max(min(179.0, float(point_y)), -179.0)

    geo_coder_api_server = 'http://geocode-maps.yandex.ru/1.x/'
    geo_coder_params = {'geocode': '{},{}'.format(point_y, point_x), 'format': 'json'}
    res = requests.get(geo_coder_api_server, params=geo_coder_params).json()

    try:
        object_raw = res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']
        coordinates = object_raw['Point']['pos']
        address = object_raw['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AddressLine']

        try:
            post_code = object_raw['metaDataProperty']['GeocoderMetaData']['Address']['postal_code']
        except KeyError:
            post_code = ''

    except IndexError:
        print('Не обнаружено')
        return ''
    except KeyError:
        print('Не обнаружено')
        return ''

    return address, post_code


def get_org_by_coords(point_x, point_y):
    search_api_server = 'https://search-maps.yandex.ru/v1/'
    api_key = '3c4a592e-c4c0-4949-85d1-97291c87825c'

    # address_ll = ', '.join(reversed('54.508265, 36.274166'.split(', ')))
    # print(address_ll)
    address_ll = '{},{}'.format(point_y, point_x)

    search_params = {
        'apikey': api_key,
        'lang': 'ru_RU',
        'll': address_ll,
        'spn': '0.0001,0.0001',
        'type': 'biz'
    }

    response = requests.get(search_api_server, params=search_params)

    json_response = response.json()

    if 'features' not in json_response:
        return False

    data = {}
    debug = []

    for org in json_response['features']:
        try:
            organization = org['properties']['CompanyMetaData']

            org_name = organization['name']
            org_address = organization['address']

            point = org['geometry']['coordinates']
            org_point = (point[1], point[0])

            dist = lonlat_distance(org_point, (point_x, point_y))

            data[dist] = (org_name, org_address, org_point, dist)
            debug.append((dist, org_name, org_address))
        except KeyError:
            pass
    # pprint(debug)

    if data:
        return data[min(data)]
    return False


pygame.init()
screen = pygame.display.set_mode((600, 450))
clock = pygame.time.Clock()

MAP_LAST = None
X, Y = 38.870962, -77.055942  # координаты
LAYER = 'map'  # короче сюда въбиваешь название слоя по кнопке
METKA = None
SPN = None
POSTAL_NEEDED = False
ORG_FOUND = False
running = True

btn_layer_1 = Button((10, 10, 80, 25), 'Схема')
btn_layer_2 = Button((95, 10, 80, 25), 'Спутник')
btn_layer_3 = Button((180, 10, 80, 25), 'Гибрид')
btn_reset = Button((500, 40, 80, 25), 'Сброс')
btn_postal = Button((10, 40, 165, 25), 'Почтовый код OFF')
search_box = TextBox((340, 10, 250, 25), '', 100)
lbl_full_name = Label((10, 420, 300, 30), '', text_color='black', background_color='-1')
gui = GUI()
gui.add_element(btn_layer_1)
gui.add_element(btn_layer_2)
gui.add_element(btn_layer_3)
gui.add_element(btn_reset)
gui.add_element(btn_postal)
gui.add_element(search_box)
gui.add_element(lbl_full_name)

search('Калуга, Суворова 160')  # В эту глобальную переменную вводится адрес который нужно найти, приделай кнопку
while running:
    for event in pygame.event.get():
        on_gui = gui.get_event(event)

        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if not search_box.active:
                handle_key_down(event.key)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if not on_gui:
                if event.button == 1:
                    draw_object(*get_ll_by_mouse(*event.pos))
                elif event.button == 3:
                    draw_org(*get_ll_by_mouse(*event.pos))

    if btn_layer_1.click:
        LAYER = 'map'
        draw(X, Y, SPN, LAYER, METKA)
    elif btn_layer_2.click:
        LAYER = 'sat'
        draw(X, Y, SPN, LAYER, METKA)
    elif btn_layer_3.click:
        LAYER = 'sat,skl'
        draw(X, Y, SPN, LAYER, METKA)
    elif btn_reset.click:
        METKA = None
        search_box.text = ''
        lbl_full_name.text = ''
        draw(X, Y, SPN, LAYER, METKA)
    elif btn_postal.click:
        POSTAL_NEEDED = not POSTAL_NEEDED
        if POSTAL_NEEDED:
            btn_postal.text = 'Почтовый код ON'
            if not ORG_FOUND:
                if lbl_full_name.text != '':
                    search(lbl_full_name.text, no_draw=True, code_only=True)
                    screen.blit(MAP_LAST, (0, 0))
        else:
            btn_postal.text = 'Почтовый код OFF'
            if not ORG_FOUND:
                if lbl_full_name.text != '':
                    if lbl_full_name.text[-6:].isdigit():
                        lbl_full_name.text = lbl_full_name.text[:-7]
                    search(lbl_full_name.text, no_draw=True, code_only=True)
                    screen.blit(MAP_LAST, (0, 0))
        btn_postal.pressed = False

    # pygame.draw.line(screen, pygame.Color('blue'), (0, 450 // 2), (600, 450 // 2))
    # pygame.draw.line(screen, pygame.Color('blue'), (600 // 2, 0), (600 // 2, 450))
    gui.update()
    gui.render(screen)
    pygame.display.flip()
    clock.tick(25)

pygame.quit()
